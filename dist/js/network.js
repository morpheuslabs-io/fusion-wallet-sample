
window.NETWORK = {
    MAINNET: {
        NAME: 'mainnet',
        URL: 'wss://mainnetpublicgateway1.fusionnetwork.io:10001',
        GATEWAY_API: 'https://mainnetapi.fusionnetwork.io',
        GATEWAY_ASIA_API: 'https://asiaapi.fusionnetwork.io',
        CHAINID: 32659
    },
    TESTNET: {
        NAME: 'testnet',
        URL: 'wss://testnetpublicgateway1.fusionnetwork.io:10001',
        GATEWAY_API: 'https://testnetapi.fusionnetwork.io',
        GATEWAY_ASIA_API: 'https://testnetasiaapi.fusionnetwork.io',
        CHAINID: 46688
    },
    MORPHEUSLABS: {
        NAME: 'morpheuslabs',
        URL: 'wss://fusion1-28244-test.morpheuslabs.io',
        GATEWAY_API: 'https://fusion1-49675-test.morpheuslabs.io',
        CHAINID: 1621
    }
}
  
 
  