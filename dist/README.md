# MyFusionWallet 
================================

In this app, we will go through steps to deploy MyFusionWallet app on Morpheuslabs BaaS platform.

Morpheuslabs platform provide great system to create a ready blockchain development with few clicks.

Now, we firstly create a workspace with all thing setup to start develop

## Step 1. Register new account in `https://bps.morpheuslabs.io`

<img src="app/images/screens/a-1.png"/>

2. Goto Membership tab click and subscribe try plan
<img src="app/images/screens/a-2.png"/>

3. Update use profile and finish subscribe
<img src="app/images/screens/a-3.png"/>
<img src="app/images/screens/a-41.png"/>
<img src="app/images/screens/a-4.png"/>

## Step 2: Download MyFusionWallet from app library.

Next we will download app from app library to run on workspace. Before you are able to download app, you need a gitlab or github repo to link with your account
1. Goto `https://gitlab.com` or `https://github.com` and register new account if you do not have it yet.
2. Goto your setting and create new access token:
<img src="app/images/screens/a-5.png"/>

`https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html`

3. Copy it and paste access token to repository setting and connect
<img src="app/images/screens/a-6.png"/>
Wait a minute for workspace to connect with your git repo
<img src="app/images/screens/a-7.png"/>

4. Goto Application Library tab
<img src="app/images/screens/a-8.png"/>

5. Click on download icon of MyFusionWallet app and download app
<img src="app/images/screens/a-9.png"/>

6. After app is downloaded, let deploy it on new workspace
<img src="app/images/screens/a-10.png"/>

7. Select workspace as fusion-wallet and deploy
<img src="app/images/screens/a-11.png"/>

Wait little for deploying app, after app is deployed, let start it by click on start icon
<img src="app/images/screens/a-12.png"/>
<img src="app/images/screens/a-13.png"/>

8. Open workspace you can see app folder is aready is setup in your workspace
<img src="app/images/screens/a-14.png"/>

So now, we have ready a development env to develop dApp
# Step 3. Create Fusion blockchain ops

Next, go to Blockchain Ops tab and click New BlockChain button
<img src="app/images/screens/b-1.png"/>

2. Select Fusion as your blockchain Ops and enter name
<img src="app/images/screens/b-2.png"/>

3. Choose Ready to Go for basic setup, or Advance for more customize options, but we select basic setup for now.
<img src="app/images/screens/b-3.png"/>

4. Enter prefix as icon and password to create blockchain ops
<img src="app/images/screens/b-4.png"/>

5. Wait a while for creating and click start ico to start node
<img src="app/images/screens/b-5.png"/>

6. Click on (i) icon to get following information, we will need it later.
    - External API URL
    - External WSS URL
<img src="app/images/screens/b-8.png"/>

7. Enter your password and click Retrieve button to get genesis address with default icon coin in it.

NOTE: Important note, when you just start up blockchain network, it will take sometime to load DAG file to start generating block, click on Explorer URL and wait until to see at least 5 block before starting app to run.
<img src="app/images/screens/b-7.png"/>


# Step 4. Install dependencies and start app

1. Goto app folder
`cd fusion-wallet`
<img src="app/images/screens/ws-1.png"/>
2. Run `npm install`

3. Install gulp `npm install gulp -g`

4. Config application, open networkConfig.js in folder `app/script/staticJS/network.js`, update following information
    - URL is 'External WS URL' you get before
    - GATEWAY_API is 'External Api URL'
    - And ChainID is 'Network Id'
    <img src="app/images/screens/ws-3.png"/>

5. Run webapp server to on port 8080 `node webServer.js`
6. Open workspace information to get public URL server
<img src="app/images/screens/ws-4.png"/>

7. Click on http-server link to view dapp
<img src="app/images/screens/ws-5.png"/>
